DRAFT: Project plan for reimplementing end-to-end task oriented dialogue system
=============================================================================

- Understand the paper https://arxiv.org/abs/1806.04441. Especially the KB attention. (6h, top priority)
- Prepare the collaboration setup and guidelines (middle priority):
    - Git repository with CI, READMES, Contribution guidelines (2h)
    - Setup slack (30m)
- Prepare artificial data (todo time, todo priority)
- Load [KWRET dataset](https://nlp.stanford.edu/blog/a-new-multi-turn-multi-domain-task-oriented-dialogue-dataset/) so it can be used later in training pipeline (todo time, todo priority)
- Prepare training loop
    - Prepare evaluation metrics


TODO FILL DRAFT
